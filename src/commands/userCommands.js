/* eslint-disable no-unused-vars */
export default async (client, channel, tags, command, args,botUser) => {
	
	if (command === 'ping') {
		client.say(channel, `@${tags.username}, "Pong!"`);
	}

	if (command === 'echo') {
		client.say(channel, `@${tags.username}, you said: "${args.join(' ')}"`);
	}

	if (command === 'command') {
		client.say(channel, `@vailable commands are: ping, pong, uptime, lurk, hug, stream and game.`);
	}

};
