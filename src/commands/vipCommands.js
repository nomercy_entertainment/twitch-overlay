import { getChannelDetailsFromName } from '../functions/apiEndpoints';
import { sanitizeStrict } from '../functions/messageparser';
import languages from '../languages';

/* eslint-disable no-unused-vars */
export default async (client, channel, tags, command, args, botUser) => {
	if (command === 'so') {
		const name = args.shift();
		if (!name) return;
		const message = args?.join(' ');

		const { userInfo, channelInfo } = await getChannelDetailsFromName(name);
		// console.log(userInfo, channelInfo);

		const twitchMessage = `Please check out ${userInfo.display_name}${message ? ', ' + message :''} at https://twitch.tv/${userInfo.display_name} ${channelInfo.game_name ? `, they are currently playing ${channelInfo.game_name}` : ``}`;

		client.say(channel, twitchMessage);

		const botMessage = `
			<div class="so-flex so-box">
				<p class="so-flex so-text">Please check out ${userInfo.display_name}${message ? ', ' + message :''}.</p>
				<div class="so-flex so-img-container" >
					<img class="so-flex so-img" src="${userInfo.profile_image_url}" />
					<div class="so-flex so-img-content">
						<h1 class="so-flex so-name">${userInfo.display_name}</h1>
						<p class="so-flex so-title"><b class="bold">Title:</b> ${sanitizeStrict(channelInfo.title)}</p>
						<p class="so-flex so-game"><b class="bold">Game:</b> ${channelInfo.game_name}</p>
						${sanitizeStrict(channelInfo.channelInfo) ? `<p class="so-flex so-info">Info: ${sanitizeStrict(channelInfo.channelInfo)}</p>` : ''}
						<p class="so-flex so-lang"><b class="bold">Lang:</b> ${languages.find((l) => l.iso_639_1 == channelInfo.broadcaster_language).english_name}</p>
					</div>
				</div>
				<p class="so-flex so-info">at <a class="so-flex so-link" src="https://twitch.tv/${name}">https://twitch.tv/${name}</a>
				</p></div>
		`;

		client.queue.now({ name: 'message', data: { user: botUser, message: botMessage } });
	}
};
