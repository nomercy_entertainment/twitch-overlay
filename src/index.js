import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { TwitchApiProvider } from './contexts/TwitchApiProvider';

export const Login = () => {};

ReactDOM.render(
	<React.StrictMode>
		<TwitchApiProvider>
			<App />
		</TwitchApiProvider>
	</React.StrictMode>,
	document.getElementById('root')
);
