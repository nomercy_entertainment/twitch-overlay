/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import Queue from './functions/messageQueue';
import Follower from './components/Follower';
import Soundboard from './components/Soundboard';
import { useTwitch } from './contexts/TwitchApiProvider.js';
import { executeCommand } from './functions/executeCommand';
import { getUserData } from './functions/getUserData';
import twitchApiClient from './functions/twitchApiClient';
import Chat from './components/Chat/Chat';
import botMessages from './botMessages';
import ViewCounter from './components/ViewCounter';
import './index.css';
import DropGame from './components/DropGame/DropGame';

function App() {
	const client = useTwitch();

	useEffect(async () => {
		
		const botUser = () => {
			return {
				uid: Math.floor(Math.random() * 10000000),
				date: new Date(Date.now()).getTime(),
				badges: [],
				profile_image_url: process.env.REACT_APP_BOT_AVATAR,
				display_name: process.env.REACT_APP_BOT_NAME,
				user_id: 1,
				is_host: false,
			}
		};
		
		if (!client) return;

		client.on('message', async (channel, tags, message, self) => {
			// console.log(channel, tags, message, self);
			if (self) return;

			if (message.startsWith('!')) {
				const args = message.slice(1).split(' ');
				const command = args.shift().toLowerCase();
				await executeCommand(client, channel, tags, command, args, botUser());
			} else {
				const user = await getUserData(tags);
				client.queue.now({ name: 'message', data: { user, message } });
			}
		});

		const interval = setInterval(() => {
			twitchApiClient.get(`/users/follows?first=1&to_id=${process.env.REACT_APP_CHANNEL_ID}`).then(({ data: { data } }) => {
				if ((Date.now() - new Date(data[0]).followed_at) / 1000 < 5000) {
					client.queue.add({ name: 'follower', data: data[0].to_name });
				}
			});
		}, 1000 * 5);

		const botInterval = setInterval(() => {
			client.queue.now({ name: 'message', data: { user: botUser(), message: botMessages[Math.floor(Math.random() * botMessages.length)].message } });
		}, 1000 * 60 * 5);

		return () => {
			clearInterval(interval);
			clearInterval(botInterval);

			client.off('message');
			client.off('raided');
			client.off('resub');
			client.off('subgift');
			client.off('submysterygift');
			client.off('subscription');
			client.off('timeout');
			client.off('clearchat');
			client.off('hosting');
			client.off('unhost');
			client.off('connecting');
			client.off('connected');
			client.off('logon');
			client.off('connectfail');
			client.off('disconnected');
			client.off('reconnect');
			client.off('join');
			client.off('part');
			client.off('crash');
			client.off("logon");
			client.off("emotesets");
		};
	}, [client]);

	return (
		<div className="App">
			<DropGame />
			<Follower />
			<Chat />
			<Soundboard />
			{/* <ViewCounter /> */}
		</div>
	);
}

export default App;
