import axios from "axios";
import React, { createContext, useContext, useEffect, useState } from "react";
import tmiJs from "tmi.js";
import Queue from "../functions/messageQueue";
import twitchApiClient from "../functions/twitchApiClient";
import LoadingInitialView from "../views/LoadingInitialView/LoadingInitialView";

export const TwitchApiContext = createContext(null);

export function useTwitch() {
	return useContext(TwitchApiContext);
}

export function TwitchApiProvider({ children }) {
	const [client, setClient] = useState();
	const [loggedIn, setLoggedIn] = useState(false);
	const [loadingText, setLoadingText] = useState('Logging in to Twitch.');

	useEffect(() => {
		console.log("logging in");

		axios
			.post(`https://id.twitch.tv/oauth2/token`, null, {
				params: {
					client_id: process.env.REACT_APP_CLIENT_ID,
					client_secret: process.env.REACT_APP_CLIENT_SECRET,
					grant_type: "client_credentials",
				},
			})
			.then(function (response) {
				localStorage.setItem(
					"access_token",
					response.data.access_token
				);
				setLoggedIn(true);
			})
			.catch(function (error) {
				console.error(error);
			});
	}, []);
	

	const [queue] = useState(new Queue());
	window.queue = queue;

	useEffect(() => {
		queue.start();
		return () => queue.stop();
	}, []);

	useEffect(async () => {
		if (!loggedIn) return;

		const newClient = tmiJs.Client({
			options: { debug: false },
			identity: {
				username: "NoMercy__Bot",
				password: `oauth:${process.env.REACT_APP_BOT_KEY}`,
			},
			channels: ["stoney_eagle"],
		});
		
		newClient.on('raided', (channel, username, viewers) => queue.now({ name: 'resub', data: { channel, username, viewers } }));
		newClient.on('resub', (channel, username, months, message, userstate, methods) => queue.now({ name: 'resub', data: { channel, username, months, message, userstate, methods } }));
		newClient.on('subgift', (channel, username, streakMonths, recipient, methods, userstate) => queue.now({ name: 'subgift', data: { channel, username, streakMonths, recipient, methods, userstate } }));
		newClient.on('submysterygift', (channel, username, numbOfSubs, methods, userstate) => queue.now({ name: 'submysterygift', data: { channel, username, numbOfSubs, methods, userstate } }));
		newClient.on('subscription', (channel, username, method, message, userstate) => queue.now({ name: 'subscription', data: { channel, username, method, message, userstate } }));
		newClient.on('timeout', (data) => queue.now({ name: 'timeout', data }));
		newClient.on('clearchat', (data) => queue.now({ name: 'clearchat', data }));
		newClient.on('hosting', (channel, viewers) => queue.now({ name: 'hosting', data: { channel, viewers } }));
		newClient.on('unhost', (channel, viewers) => queue.now({ name: 'unhost', data: { channel, viewers } }));
		newClient.on('connecting', (address, port) => queue.now({ name: 'connecting', data: { address, port } }));
		newClient.on('connected', (address, port) => queue.now({ name: 'connected', data: { address, port } }));
		newClient.on('logon', (data) => queue.now({ name: 'logon', data }));
		newClient.on('connectfail', (data) => queue.now({ name: 'connectfail', data }));
		newClient.on('disconnected', (reason) => queue.now({ name: 'disconnected', data: { reason } }));
		newClient.on('reconnect', (data) => queue.now({ name: 'reconnect', data }));
		newClient.on('join', (channel, username) => queue.now({ name: 'join', data: { channel, username } }));
		newClient.on('part', (channel, username) => queue.now({ name: 'part', data: { channel, username } }));
		newClient.on('crash', (data) => queue.now({ name: 'crash', data }));
		
		newClient.on("logon", () => {
			setLoadingText('Logged in....');
			newClient.once("join", () => {
				setLoadingText('');
			});
		});

		newClient._globalEmotes = {};
		await twitchApiClient.get(`chat/emotes/global`).then(({ data: { data } }) => {
			data.map(e => {
				newClient._globalEmotes[e.name] = e.images.url_2x;
				return;
			});
		});
		newClient._channelEmotes = {};

		await twitchApiClient.get(`chat/emotes`, {
			params: {
				broadcaster_id: process.env.REACT_APP_CHANNEL_ID
			}
		}).then(({ data: { data } }) => {
			data.map(e => {
				newClient._channelEmotes[e.name] = e.images.url_2x;
				return;
			});
		});

		newClient.queue = queue;

		newClient.connect();

		setClient(newClient);

		return newClient.disconnect();
		
	}, [loggedIn]);

	return (
		<TwitchApiContext.Provider value={client}>
			<LoadingInitialView text={loadingText}/>
			{children}
		</TwitchApiContext.Provider>
	);
}
