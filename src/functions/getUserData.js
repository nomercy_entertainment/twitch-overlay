import badgeLinks from "../badges.json";
import twitchApiClient from "./twitchApiClient";

const users = [];

export const getUserData = async (tags) => {

	if(users.some(u => u.user_id == tags["user-id"])) {
		return {
			...tags,
			uid: tags.id,
			date: new Date(Date.now()).getTime(),
			...users.find(u => u.user_id == tags["user-id"]),
		}
	}

	const { data:{data} } = await twitchApiClient.get(
		`/users?id=${tags["user-id"]}`
	);

	const badges = Object.entries(tags.badges ?? []).map((b) => {
		const [key, val] = b;
		return `${badgeLinks["badge_sets"][key].versions[val].image_url_2x}`;
	}) ?? [];

	const user = {
		badges: badges,
		profile_image_url: data[0].profile_image_url,
		display_name: tags["display-name"],
		user_id: tags["user-id"],
		is_host: tags["user-id"] == tags["room-id"],
	};

	users.push(user);

	return {
		...tags,
		uid: tags.id,
		date: new Date(Date.now()).getTime(),
		...user,
	};
};
