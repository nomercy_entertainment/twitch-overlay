import twitchApiClient from './twitchApiClient';

export const getUserIdFromName = async (name, limit = 1) => {
	const {
		data: { data: user },
	} = await twitchApiClient.get(`users?login=${name}`);

	return user.slice(0, limit);
};

export const getChannelDetailsFromName = async (name) => {
	const user = await getUserIdFromName(name, 1);
	const {
		data: { data },
	} = await twitchApiClient.get(`channels?broadcaster_id=${user[0].id}`);

	return {
		channelInfo: data[0],
		userInfo: user[0]
	};
};
