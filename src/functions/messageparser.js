
import DOMPurify from 'dompurify';

export default (message, emotes) => {
	message = sanitize(message);

	if (!emotes) return message;
	const stringReplacements = [];

	Object.entries(emotes).forEach(([id, positions]) => {
        
		const position = positions[0];
		const [start, end] = position.split('-');
		const stringToReplace = message.substring(parseInt(start, 10), parseInt(end, 10) + 1);

		stringReplacements.push({
			stringToReplace: stringToReplace,
			replacement: `<img 
				src="https://static-cdn.jtvnw.net/emoticons/v2/${id}/default/dark/2.0" 
				style="width:30px;height:30px;transform:translateY(25%);"
			>`,
		});
	});

	const messageHTML = stringReplacements.reduce((acc, { stringToReplace, replacement }) => {
		return acc.split(stringToReplace).join(replacement);
	}, message);
	
	return messageHTML;
}

export const sanitize = (message) => {
	return DOMPurify.sanitize(message, {
		FORBID_ATTR: ['style', 'onerror', 'onload', 'class', 'width', 'height', 'dir'],
		ALLOWED_TAGS: ['img','br', 'p', 'a','marquee'],
	});
}

export const sanitizeStrict = (message) => {
	return DOMPurify.sanitize(message?.replace(/</g, '&lt;')?.replace(/>/g, '&gt;'), {
		ALLOWED_TAGS: [],
		ALLOWED_ATTR: [],
	});
}
