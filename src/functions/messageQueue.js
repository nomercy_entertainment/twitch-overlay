function Queue() {
	this.elements = [];
	this.interval;
	// this.follower = (data) =>
	// 	new CustomEvent('follower', {detail: data});
	// this.message = (data) =>
	// 	new CustomEvent('message', {detail: data});
	this.event = (name, data) => new CustomEvent(name, { detail: data });
	this.all = (data) => new CustomEvent('all', { detail: data });
}

Queue.prototype.add = function (e) {
	this.elements.push(e);
};

Queue.prototype.dequeue = function () {
	const event = this.elements.shift();
	const { name, data } = event;

	document.dispatchEvent(this.event(name, data));
	document.dispatchEvent(this.all(event));
};

Queue.prototype.now = function (e) {
	const { name, data } = e;

	document.dispatchEvent(this.event(name, data));
	document.dispatchEvent(this.all(e));
};

Queue.prototype.isEmpty = function () {
	return this.elements.length == 0;
};

Queue.prototype.peek = function () {
	return !this.isEmpty() ? this.elements[0] : undefined;
};

Queue.prototype.length = function () {
	return this.elements.length;
};

Queue.prototype.start = function () {
	this.interval = setInterval(() => {
		if (this.isEmpty()) return;
		this.dequeue();
	}, 100);
};

Queue.prototype.stop = function () {
	clearInterval(this.interval);
};

export default Queue;
