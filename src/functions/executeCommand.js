import hostCommands from '../commands/hostCommands';
import modCommands from '../commands/modCommands';
import userCommands from '../commands/userCommands';
import vipCommands from '../commands/vipCommands';

export const executeCommand = async (client, channel, tags, command, args, botUser) => {
	console.log(channel, tags, command, args, botUser);

	if (tags.badges?.broadcaster) {
		hostCommands(client, channel, tags, command, args, botUser);
	}

	if (tags.badges?.broadcaster || tags.badges?.moderator) {
		modCommands(client, channel, tags, command, args, botUser);
	}

	if (tags.badges?.broadcaster || tags.badges?.moderator || tags.badges?.vip) {
		vipCommands(client, channel, tags, command, args, botUser);
	}

	userCommands(client, channel, tags, command, args, botUser);
};
