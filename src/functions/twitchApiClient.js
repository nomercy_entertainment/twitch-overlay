import axiosRetry from "axios-retry";
import axios from "axios";

const onRequest = (config) => {
	config.headers = {
		Authorization: `Bearer ${localStorage.getItem("access_token")}`,
		"Client-Id": process.env.REACT_APP_CLIENT_ID,
	};
	config.timeout = 1000;
	config.params = {
		...config.params,
	};
	return config;
};

const onRequestError = (error) => {
	// console.error(error);

	return Promise.reject(error);
};

const onResponse = (response) => {
	// console.info(response.data);

	return response;
};

const onResponseError = (error) => {
	// console.error(error);

	return Promise.reject(error.response?.data);
};

export function setupInterceptorsTo(axiosInstance) {
	axiosInstance.interceptors.request.use(onRequest, onRequestError);
	axiosInstance.interceptors.response.use(onResponse, onResponseError);
	return axiosInstance;
}

const twitchApiClient = setupInterceptorsTo(
	axios.create({
		baseURL: `https://api.twitch.tv/${process.env.REACT_APP_TWITCH_API_VERSION}/`,
	})
);

axiosRetry(twitchApiClient, {
	retries: 3,
});

export default twitchApiClient;
