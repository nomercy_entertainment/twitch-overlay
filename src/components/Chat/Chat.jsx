/* eslint-disable no-unused-vars */
import React, { useEffect, useRef, useState } from 'react';
import messageparser from '../../functions/messageparser';
import Message from '../Message/Message';
import useStyles from './styles';

function Chat() {
	const [messages, setMessages] = useState([]);
	const ref = useRef();

	const classes = useStyles();

	const displayMessage = ({ detail }) => {
		if(detail.user.display_name != process.env.REACT_APP_BOT_NAME){
			detail.message = messageparser(detail.message, detail.user.emotes);
		}
		setMessages((prevMessages) => {
			return [
				...prevMessages.slice(Math.max(prevMessages.length - 9, 0)),
				{
					...detail.user,
					message: detail.message,
				},
			];
		});
	};

	useEffect(() => {
		setMessages([]);
		document.addEventListener('message', displayMessage);

		return () => {
			document.removeEventListener('message', displayMessage);
			setMessages([]);
		};
	}, []);

	useEffect(() => {
		if (messages.length == 0) return;
		console.log(messages);
		setTimeout(() => {
			ref.current?.lastChild?.scrollIntoView();
		}, 50);
	}, [messages]);

	return (
		<div className={classes.chat} ref={ref}>
			{messages.map((m) => (
				<Message key={`message-${m.uid}`} message={m} />
			))}
		</div>
	);
}

export default Chat;
