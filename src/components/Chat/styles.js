import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
	chat: {
		'--scrollbar-width': '7px',
		width: '27.5vw',
		height: '75vh',
		alignSelf: 'flex-end',
		position: 'absolute',
		right: 0,
		bottom: 0,
		display: 'flex',
		flexDirection: 'column-reverse',
		overflowY: 'auto',
		overflowX: 'hidden',
		gap: 4,
		padding: 4,
	},
}));

export default useStyles;
