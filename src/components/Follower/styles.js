import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
	follower: {
		'--size': '40',
		background: 'rgb(50, 50, 50)',
		color: '#fff',
		fontSize: '3vw',
		fontWeight: 'bold',
		fontamily: "Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif",
		width: 'calc(var(--size) * 1vw)',
		height: 'calc((var(--size) * 1vw) / 8 * 2)',
		position: 'relative',
		top: '2rem',
		left: '50%',
		transform: 'translateX(-50%)',
		display: 'flex',
	},

	logo: {
		width: '20%',
		height: '100%',
	},

	svg: {
		width: '100%',
		height: '100%',
	},

	name: {
		fontSize: '2.5vw',
	},

	content: {
		height: '100%',
		width: '80%',
		display: 'flex',
		justifyWontent: 'center',
		alignItems: 'center',
		flexDirection: 'column',
	},

	sounds: {
		position: 'absolute',
		width: '800px',
		height: '600px',
		left: '50vw',
		top: '2rem',
		transform: 'translateX(-50%)',
	},
}));

export default useStyles;
