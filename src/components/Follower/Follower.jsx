import React, { useEffect, useState } from 'react';
import Logo from '../Logo';
let timeout;

function Follower() {

	const [latestFollower, setLatestFollower] = useState('');
	const [display, setDisplay] = useState(false);

	const displayFollower = ({ detail }) => {
		// console.log(detail);

		setLatestFollower(detail);
		setDisplay(true);

		timeout = setTimeout(() => {
			setDisplay(false);
		}, 5000);
	};

	useEffect(() => {
		document.addEventListener('follower', displayFollower);

		return () => {
			clearTimeout(timeout);
			document.removeEventListener('follower', displayFollower);
		};
	}, []);

	return (
		<div
			className="follower"
			style={{
				display: display ? 'flex' : 'none',
			}}
		>
			<div className="logo">
				<Logo />
			</div>
			<div className="content">
				<div className="name">{latestFollower}</div>
				<div className="text">Just Followed!</div>
			</div>
		</div>
	);
}

export default Follower;
