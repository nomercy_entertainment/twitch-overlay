/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import useStyles from './styles';
import TimeAgo from 'javascript-time-ago';
import en from 'javascript-time-ago/locale/en';
import botMessages from '../../botMessages';

function Message({ message: m }) {
	TimeAgo.addLocale(en);
	const timeAgo = new TimeAgo(['en-US', 'en']);

	const classes = useStyles();

	const [time, setTime] = useState(null);
	const [message, setMessage] = useState('');
	const [fade, setFade] = useState(false);

	useEffect(() => {
		setTime(timeAgo.format(Date.now() - (Date.now() - m.date)));
		const interval = setInterval(() => {
			setTime(timeAgo.format(Date.now() - (Date.now() - m.date)));
			setFade(true);
		}, 1000 * 29);

		return () => clearInterval(interval);
	}, []);

	useEffect(() => {
		setMessage(m.message);
	}, [m]);

	return (
		<div
			className={classes.card}
			key={Date.now()}
			style={{
				background: `${m.color}ee`,
				display: fade ? 'none' : '',
			}}
		>
			<div className={classes.top}>
				<img
					className={classes.avatar}
					src={m.profile_image_url}
					style={{
						padding: m.display_name == process.env.REACT_APP_BOT_NAME ? 5 : '',
						background: m.display_name == process.env.REACT_APP_BOT_NAME ? 'black' : '',
					}}
				/>
				{m.badges?.map((b) => (
					<img className={classes.badge} key={b} src={b} />
				))}
				<span
					className={classes.userName}
					style={{
						color: 'fff',
					}}
				>
					{m.display_name}
				</span>{' '}
			</div>
			<div className={classes.content}>
				<span
					className={classes.message}
					dangerouslySetInnerHTML={{ __html: message }}
					style={{
						...botMessages.find((m) => m.message == message)?.style,
					}}
				/>
			</div>
			<div className={classes.footer}>
				<div className={classes.footerMessage}></div>
				<div className={classes.time}>{time}</div>
			</div>
		</div>
	);
}

export default Message;
