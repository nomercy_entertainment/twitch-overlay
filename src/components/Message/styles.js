import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
	card: {
		background: '#222222',
		width: '100%',
		display: 'flex',
		flexDirection: 'column',
		color: 'white',
		fontSize: 20,
		padding: 16,
		borderRadius: 8,
		transition: 'all 2s ease-in-out',
	},
	top: {
		display: 'flex',
		flexDirection: 'row',
		gap: 8,
	},
	content: {
		marginTop: 8,
	},
	badge: {
		height: 20,
		alignSelf: 'center',
	},
	avatar: {
		height: 50,
		borderRadius: '50%',
	},
	userName: {
		lineHeight: '50px',
		placeSelf: 'center',
	},
	message: {
		wordBreak: 'break-word',
		alignItems: 'center',
		gap: 4,
		maxWidth: 485,
		overflow: 'hidden',
		'& a': {
			color: 'var(--theme-color)',
		},
		'& h1': {
			margin: '5px 0',
		},
		'& p': {
			margin: '5px 0',
			fontSize: 18,
		},
		'& *': {
			fontSize: 20,
			direction: 'ltr !important',
			maxWidth: '485px !important',
		},

		'& .so-flex': {
			display: 'flex',
			flexWrap: 'nowrap',
		},

		'& .so-box': {
			flexDirection: 'column',
			flexWrap: 'wrap',
			display: 'flex !important',
		},

		'& .so-link': {
			marginLeft: '16px',
		},

		'& .so-img-container': {
			display: 'flex',
			flexDirection: 'row',
			flexWrap: 'nowrap',
			background: '#303030',
			padding: '8px 4px',
			margin: '8px 0',
			borderRadius: '8px',
		},

		'& .so-img': {
			width: '150px',
			height: '150px',
			borderRadius: '10%',
			margin: '0 8px',
			alignSelf: 'center',
		},

		'& .so-img-content': {
			display: 'flex',
			flexDirection: 'column',
			flexWrap: 'nowrap',
			marginLeft: '8px',
		},

		'& .so-name': {
			fontSize: '28px !important',
			alignSelf: 'center',
		},

		'& .so-title': {
			fontSize: '18px',
		},

		'& .so-game': {
			fontSize: '18px',
		},

		'& .so-info': {
			fontSize: '18px',
		},

		'& .so-lang': {
			fontSize: '18px',
		},

		'& .bold': {
			width: '70px',
			minWidth: '70px',
			maxWidth: '70px',
		},
	},
	footer: {
		display: 'flex',
		justifyContent: 'space-between',
		marginTop: 8,
		gap: 8,
	},
	footerMessage: {
		width: '100%',
	},
	time: {
		fontSize: 14,
		textAlign: 'end',
		whiteSpace: 'nowrap',
	},
}));

export default useStyles;
