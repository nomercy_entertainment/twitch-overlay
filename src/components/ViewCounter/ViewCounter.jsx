import React from 'react';
import useStyles from './styles';

function ViewCounter() {
	const classes = useStyles();

	return (
		<div className={classes.counter}>
			<iframe title="counter" src="https://streamlabs.com/widgets/viewer-count?token=B6192ED344DC9671AD94" height="100%" width="100%" frameBorder="0" />
		</div>
	);
}

export default ViewCounter;
