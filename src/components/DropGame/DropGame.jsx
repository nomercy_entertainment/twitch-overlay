import React from 'react';
import useStyles from './styles';

function DropGame() {
	const classes = useStyles();

	return <iframe className={classes.dropGame} src="https://twitchdrop.kanawanagasaki.ru/?channel=stoney_eagle" />;
}

export default DropGame;
