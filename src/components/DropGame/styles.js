import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
	dropGame: {
		position: 'absolute',
		inset: 0,
		width: '100vw',
		height: '100vh',
	}
}));

export default useStyles;
