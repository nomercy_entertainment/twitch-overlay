import React from "react";

import useStyles from './styles';

function Soundboard() {
	const classes = useStyles();
	return (
		<div className={classes.sounds}>
			<iframe
				title="sounds"
				src="https://source.soundalerts.com/alert/c33844e1-43ef-3ab7-b332-bfb9c886ed3a"
				height="100%"
				width="100%"
				frameBorder="0"
			/>
		</div>
	);
}

export default Soundboard;
