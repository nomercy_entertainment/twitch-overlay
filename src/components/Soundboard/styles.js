import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
	sounds: {
		width: 480,
		height: 800,
		margin: 'auto',
	},
}));

export default useStyles;
