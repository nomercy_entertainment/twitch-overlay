import React from 'react';
import useStyles from './styles';

function Goal() {
	const classes = useStyles();

	return (
		<div className={classes.goal}>
			<iframe title="counter" src="https://dashboard.twitch.tv/widgets/goal/stoney_eagle" height="100%" width="100%" frameBorder="0" />
		</div>
	);
}

export default Goal;
