import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
	rootParent: (props) => ({
		display: props.text ? 'flex' : 'none',
		zIndex: 9999999,
		position: 'absolute',
		overflow: 'hidden',
		flexDirection: 'column',
		justifyContent: 'center',
		width: '100vw',
		height: '100vh',
		paddingBottom: '17.4vh',
		alignItems: 'center',
		background: '#000',
	}),
	svgRoot: {
		width: '100%',
		maxHeight: 150,
		maxWidth: '80vw',
		marginBottom: 20,
	},
	rootLoading: {
		display: 'flex',
		top: '50vh',
		left: '50vw',
		textAlign: 'center',
		zIndex: 99999999,
		'& span': {
			display: 'flex',
			verticalAlign: 'middle',
			width: 10,
			height: 10,
			margin: 5,
			background: '#fff',
			borderRadius: '50%',
			animation: `$loading 1s infinite alternate`,
			outline: '2px ​solid rgba(0, 0, 0, 0.2)',

			'&:nth-of-type(1)': {
				background: 'var(--color-750, #fff)',
			},
			'&:nth-of-type(2)': {
				background: 'var(--color-650, #266ef6)',
				animationDelay: '0.2s',
			},
			'&:nth-of-type(3)': {
				background: 'var(--color-550, #bf00ff)',
				animationDelay: '0.4s',
			},
			'&:nth-of-type(4)': {
				background: 'var(--color-450, #ff8b00)',
				animationDelay: '0.6s',
			},
			'&:nth-of-type(5)': {
				background: 'var(--color-350, #c60404)',
				animationDelay: '0.8s',
			},
			'&:nth-of-type(6)': {
				background: 'var(--color-150, #ffd300)',
				animationDelay: '1s',
			},
			'&:nth-of-type(7)': {
				background: 'var(--color-50, #35b535)',
				animationDelay: '1.2s',
			},
		},
	},
	'@keyframes loading': {
		'0%': {
			opacity: 0,
		},
		'100%': {
			opacity: 1,
		},
	},
}));

export default useStyles;
