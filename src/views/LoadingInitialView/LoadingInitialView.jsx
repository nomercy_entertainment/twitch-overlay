import React from 'react';
import useStyles from './styles';

function LoadingInitialView({text}) {

	const classes = useStyles({text});

	return (
		<div className={classes.rootParent}>
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="20 70 1100 250" className={classes.svgRoot}>
				<g>
					<text
						transform="translate(355 297.39)"
						style={{
							isolation: 'isolate',
							fontSize: `calc(54.6px + 100%)`,
							fill: 'var(--theme-color)',
							fontFamily: 'Helvetica, Arial, sans-serif',
							fontWeight: 700,
							letterSpacing: '.8px',
						}}
					>
						{text}
					</text>
					<text
						transform="translate(345 202.4)"
						style={{
							isolation: 'isolate',
							fontSize: '136.314px',
							letterSpacing: '2px',
							fill: 'rgb(255, 255, 255)',
							fontFamily: 'Helvetica, Arial, sans-serif',
							fontWeight: 700,
						}}
					>
						Stoney_Eagle
					</text>
					<polygon
						points="324.65 91.98 324.65 300.15 286.66 300.15 286.66 142.5 253.72 172.27 240.74 184.88 202.71 146.01 202.71 300.15 167.13 254.69 164 250.93 173.59 244.57 140.38 222.6 89.63 161.7 89.63 300.15 51.64 300.15 51.64 92.26 89.15 92.26 89.63 92.86 164.16 188.76 164.16 91.98 202.71 91.98 202.71 92.56 203.03 92.26 241.89 131.99 286.03 92.12 286.66 92.8 286.66 91.98 324.65 91.98"
						style={{ fill: 'var(--theme-color)' }}
					></polygon>
					<g>
						<rect x="202.7" y="188.21" width="22.84" height="111.94" style={{ fill: 'rgb(255, 255, 255)' }}></rect>
						<rect x="263.82" y="188.21" width="22.84" height="111.94" style={{ fill: 'rgb(255, 255, 255)' }}></rect>
					</g>
					<polygon points="173.59 244.57 164 250.93 89.63 300.15 89.63 189 140.38 222.6 173.59 244.57" style={{ fill: 'rgb(255, 255, 255)' }}></polygon>
				</g>
			</svg>
			<div className={classes.rootLoading}>
				<span></span>
				<span></span>
				<span></span>
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
		</div>
	);
}

export default LoadingInitialView;
