export default [
	{
		name: 'nomercy',
		message: `███╗⠀⠀⠀██╗⠀██████╗⠀███╗⠀⠀⠀███╗███████╗██████╗⠀⠀██████╗██╗⠀⠀⠀██╗
    ████╗⠀⠀██║██╔═══██╗████╗⠀████║██╔════╝██╔══██╗██╔════╝╚██╗⠀██╔╝
    ██╔██╗⠀██║██║⠀⠀⠀██║██╔████╔██║█████╗⠀⠀██████╔╝██║⠀⠀⠀⠀⠀⠀╚████╔╝⠀
    ██║╚██╗██║██║⠀⠀⠀██║██║╚██╔╝██║██╔══╝⠀⠀██╔══██╗██║⠀⠀⠀⠀⠀⠀⠀╚██╔╝⠀⠀
    ██║⠀╚████║╚██████╔╝██║⠀╚═╝⠀██║███████╗██║⠀⠀██║╚██████╗⠀⠀⠀██║⠀⠀⠀
    ╚═╝⠀⠀╚═══╝⠀╚═════╝⠀╚═╝⠀⠀⠀⠀⠀╚═╝╚══════╝╚═╝⠀⠀╚═╝⠀╚═════╝⠀⠀⠀╚═╝⠀⠀`,
		style: {
			fontSize: 8,
			lineHeight: '6px',
		},
	},
	{
		name: 'animeWink',
		message: `⣿⡇⣿⣿⣿⠛⠁⣴⣿⡿⠿⠧⠹⠿⠘⣿⣿⣿⡇⢸⡻⣿⣿⣿⣿⣿⣿⣿
        ⢹⡇⣿⣿⣿⠄⣞⣯⣷⣾⣿⣿⣧⡹⡆⡀⠉⢹⡌⠐⢿⣿⣿⣿⡞⣿⣿⣿
        ⣾⡇⣿⣿⡇⣾⣿⣿⣿⣿⣿⣿⣿⣿⣄⢻⣦⡀⠁⢸⡌⠻⣿⣿⣿⡽⣿⣿
        ⡇⣿⠹⣿⡇⡟⠛⣉⠁⠉⠉⠻⡿⣿⣿⣿⣿⣿⣦⣄⡉⠂⠈⠙⢿⣿⣝⣿
        ⠤⢿⡄⠹⣧⣷⣸⡇⠄⠄⠲⢰⣌⣾⣿⣿⣿⣿⣿⣿⣶⣤⣤⡀⠄⠈⠻⢮
        ⠄⢸⣧⠄⢘⢻⣿⡇⢀⣀⠄⣸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⡀⠄⢀
        ⠄⠈⣿⡆⢸⣿⣿⣿⣬⣭⣴⣿⣿⣿⣿⣿⣿⣿⣯⠝⠛⠛⠙⢿⡿⠃⠄⢸
        ⠄⠄⢿⣿⡀⣿⣿⣿⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣿⣿⣿⣿⡾⠁⢠⡇⢀
        ⠄⠄⢸⣿⡇⠻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣏⣫⣻⡟⢀⠄⣿⣷⣾
        ⠄⠄⢸⣿⡇⠄⠈⠙⠿⣿⣿⣿⣮⣿⣿⣿⣿⣿⣿⣿⣿⡿⢠⠊⢀⡇⣿⣿
        ⠒⠤⠄⣿⡇⢀⡲⠄⠄⠈⠙⠻⢿⣿⣿⠿⠿⠟⠛⠋⠁⣰⠇⠄⢸⣿⣿⣿
        ⠄⠄⠄⣿⡇⢬⡻⡇⡄⠄⠄⠄⡰⢖⠔⠉⠄⠄⠄⠄⣼⠏⠄⠄⢸⣿⣿⣿
        ⠄⠄⠄⣿⡇⠄⠙⢌⢷⣆⡀⡾⡣⠃⠄⠄⠄⠄⠄⣼⡟⠄⠄⠄⠄⢿⣿⣿`,
		style: {
		},
	},
];
